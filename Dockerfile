
############
### prod ###
############

# base image
FROM nginx:latest

ARG TARGETPLATFORM
ARG BUILDPLATFORM
RUN echo "I am running on $BUILDPLATFORM, building for $TARGETPLATFORM" > /log

# copy artifact build from the 'build environment'
COPY index.html /usr/share/nginx/html

# expose port 80
EXPOSE 8080

# run nginx
CMD ["nginx", "-g", "daemon off;"]
